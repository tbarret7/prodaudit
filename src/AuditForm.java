/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tbarrett
 */


import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import java.awt.print.PrinterJob;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.print.PrinterJob;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PageFormat;
import java.awt.Graphics2D;
import javax.swing.JOptionPane;

public class AuditForm extends javax.swing.JFrame {

    private static void layoutComponent(JComponent component) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Connection con = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    private String geiItem = null;
    private String geiRev = null;
    private byte[] image;
    /**
     * Creates new form AuditForm
     */
    
    public AuditForm() { //onload
        initComponents();
        SQLConnect conn = new SQLConnect();
        jPanel5.getRootPane().setDefaultButton(searchRecipeButton);
        try {
            
        auditorNameText.setText(System.getenv("USERNAME").toString());              
        con = conn.SQLConnect();
        String descQry = "Select Description from tblDescriptions";
        String defectDescriptionQry = "Select DefectDescription from tblDefectDescriptions";
        String checkMethodQry = "Select CheckMethod from tblCheckMethods";
        
        stmt = con.createStatement();
        ResultSet rsDescriptions = stmt.executeQuery(descQry);
                
        while (rsDescriptions.next()){
                 
            descText1.addItem(rsDescriptions.getString("Description"));
            descText2.addItem(rsDescriptions.getString("Description"));
            descText3.addItem(rsDescriptions.getString("Description"));
            descText4.addItem(rsDescriptions.getString("Description"));
            descText5.addItem(rsDescriptions.getString("Description"));
            descText6.addItem(rsDescriptions.getString("Description"));
            descText7.addItem(rsDescriptions.getString("Description"));
            descText8.addItem(rsDescriptions.getString("Description"));
            descText9.addItem(rsDescriptions.getString("Description"));
            descText10.addItem(rsDescriptions.getString("Description"));
            
        }        
        stmt.close();
        rsDescriptions.close();
        
        Statement statement = null;
        statement = con.createStatement();
        ResultSet rsCheckMethods = statement.executeQuery(checkMethodQry);
        
        while (rsCheckMethods.next()){
            checkMethodText1.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText2.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText3.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText4.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText5.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText6.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText7.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText8.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText9.addItem(rsCheckMethods.getString("CheckMethod"));
            checkMethodText10.addItem(rsCheckMethods.getString("CheckMethod"));
            
                    
        }
        statement.close();
        rsCheckMethods.close();
        
        Statement stmnt = null;
        stmnt = con.createStatement();
        ResultSet rsDefectDescriptions = stmnt.executeQuery(defectDescriptionQry);
        
        while (rsDefectDescriptions.next()){
            
            defectDescText1.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText2.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText3.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText4.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText5.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText6.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText7.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText8.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText9.addItem(rsDefectDescriptions.getString("DefectDescription"));
            defectDescText10.addItem(rsDefectDescriptions.getString("DefectDescription"));
            
        }
        stmnt.close();
        con.close();
        rsDefectDescriptions.close();
        }
        catch (Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        newRecipeButton = new javax.swing.JButton();
        reportButton = new javax.swing.JButton();
        auditViewerButton = new javax.swing.JButton();
        printAuditButton = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        workOrderNumberText = new javax.swing.JTextField();
        lineNumberText = new javax.swing.JTextField();
        operationNumberText = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        searchRecipeButton = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        auditTypeText = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        qtyOrderedText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        partNumberText = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        dieNumberText = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        revText = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        customerText = new javax.swing.JTextField();
        customerPartNumberText = new javax.swing.JTextField();
        customerRevText = new javax.swing.JTextField();
        orderlineText = new javax.swing.JTextField();
        operationSeqText = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        lotSizeText = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        sampleSizeText = new javax.swing.JComboBox<>();
        inspectionLevelText = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        shiftNumberText = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        descText1 = new javax.swing.JComboBox<>();
        jLabel19 = new javax.swing.JLabel();
        descText2 = new javax.swing.JComboBox<>();
        descText3 = new javax.swing.JComboBox<>();
        descText4 = new javax.swing.JComboBox<>();
        descText5 = new javax.swing.JComboBox<>();
        descText6 = new javax.swing.JComboBox<>();
        descText7 = new javax.swing.JComboBox<>();
        descText8 = new javax.swing.JComboBox<>();
        descText9 = new javax.swing.JComboBox<>();
        descText10 = new javax.swing.JComboBox<>();
        jLabel20 = new javax.swing.JLabel();
        checkMethodText1 = new javax.swing.JComboBox<>();
        checkMethodText2 = new javax.swing.JComboBox<>();
        checkMethodText3 = new javax.swing.JComboBox<>();
        checkMethodText4 = new javax.swing.JComboBox<>();
        checkMethodText5 = new javax.swing.JComboBox<>();
        checkMethodText6 = new javax.swing.JComboBox<>();
        checkMethodText7 = new javax.swing.JComboBox<>();
        checkMethodText8 = new javax.swing.JComboBox<>();
        checkMethodText9 = new javax.swing.JComboBox<>();
        checkMethodText10 = new javax.swing.JComboBox<>();
        jLabel21 = new javax.swing.JLabel();
        qtySampledText1 = new javax.swing.JTextField();
        qtySampledText2 = new javax.swing.JTextField();
        qtySampledText3 = new javax.swing.JTextField();
        qtySampledText4 = new javax.swing.JTextField();
        qtySampledText5 = new javax.swing.JTextField();
        qtySampledText6 = new javax.swing.JTextField();
        qtySampledText7 = new javax.swing.JTextField();
        qtySampledText8 = new javax.swing.JTextField();
        qtySampledText9 = new javax.swing.JTextField();
        qtySampledText10 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        qtyDiscText1 = new javax.swing.JTextField();
        qtyDiscText2 = new javax.swing.JTextField();
        qtyDiscText3 = new javax.swing.JTextField();
        qtyDiscText4 = new javax.swing.JTextField();
        qtyDiscText5 = new javax.swing.JTextField();
        qtyDiscText6 = new javax.swing.JTextField();
        qtyDiscText7 = new javax.swing.JTextField();
        qtyDiscText8 = new javax.swing.JTextField();
        qtyDiscText9 = new javax.swing.JTextField();
        qtyDiscText10 = new javax.swing.JTextField();
        defectDescText1 = new javax.swing.JComboBox<>();
        defectDescText2 = new javax.swing.JComboBox<>();
        defectDescText3 = new javax.swing.JComboBox<>();
        defectDescText4 = new javax.swing.JComboBox<>();
        defectDescText5 = new javax.swing.JComboBox<>();
        defectDescText6 = new javax.swing.JComboBox<>();
        defectDescText7 = new javax.swing.JComboBox<>();
        defectDescText8 = new javax.swing.JComboBox<>();
        defectDescText9 = new javax.swing.JComboBox<>();
        defectDescText10 = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        defectSummaryText = new javax.swing.JTextArea();
        dispositionText = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        auditorNameText = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        closedDateText = new javax.swing.JTextField();
        inspNumberText = new javax.swing.JTextField();
        dmrNumberText = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Product Audit");

        newRecipeButton.setText("New Recipe");
        newRecipeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newRecipeButtonActionPerformed(evt);
            }
        });

        reportButton.setText("Audit Labels");
        reportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportButtonActionPerformed(evt);
            }
        });

        auditViewerButton.setText("Audit Viewer");
        auditViewerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                auditViewerButtonActionPerformed(evt);
            }
        });

        printAuditButton.setText("Print Audit");
        printAuditButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printAuditButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(reportButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(newRecipeButton, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(printAuditButton, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(auditViewerButton, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newRecipeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(printAuditButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(reportButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(auditViewerButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        jLabel26.setText("Search Recipe by WO");

        jLabel27.setText("Work Order Number:");

        jLabel28.setText("Line Number:");

        jLabel29.setText("Operation:");

        searchRecipeButton.setText("Search Recipes");
        searchRecipeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchRecipeButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel26)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(operationNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lineNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(workOrderNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(47, 47, 47))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(searchRecipeButton)
                .addGap(31, 31, 31))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lineNumberText, operationNumberText, workOrderNumberText});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(workOrderNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lineNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(operationNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(searchRecipeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        auditTypeText.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Incoming", "Dock Audit", "In-Process final" }));
        auditTypeText.setName("auditTypeText"); // NOI18N

        jLabel1.setText("Audit Type");

        qtyOrderedText.setName("qtyOrderedText"); // NOI18N

        jLabel2.setText("Quantity Ordered");
        jLabel2.setToolTipText("");

        partNumberText.setName("partNumberText"); // NOI18N

        jLabel3.setText("GEI P/N");
        jLabel3.setToolTipText("");

        dieNumberText.setName("dieNumberText"); // NOI18N

        jLabel4.setText("Die");
        jLabel4.setToolTipText("");

        revText.setName("revText"); // NOI18N

        jLabel5.setText("REV");
        jLabel5.setToolTipText("");

        jLabel6.setText("Customer");

        jLabel7.setText("Cust P/N");

        jLabel8.setText("Cust Rev");

        jLabel9.setText("Orderline");

        jLabel10.setText("Oper Seq");

        jLabel11.setText("Lot Size");

        jLabel12.setText("Sample Size");

        sampleSizeText.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0.65 AQL", "1.0 AQL", "1.5 AQL", "2.5 AQL", "100%" }));

        inspectionLevelText.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TIGHTENED", "NORMAL", "REDUCED", "ZERO ACCEPTANCE", "100%" }));

        jLabel13.setText("Inspection Level");

        jLabel14.setText("Shift No.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(revText, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(partNumberText)
                    .addComponent(qtyOrderedText)
                    .addComponent(auditTypeText, 0, 1, Short.MAX_VALUE)
                    .addComponent(dieNumberText, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(customerPartNumberText, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                                    .addComponent(customerRevText)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(orderlineText, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                            .addComponent(operationSeqText))))
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel11)
                    .addComponent(jLabel14))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(shiftNumberText, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sampleSizeText, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(inspectionLevelText, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lotSizeText))
                .addContainerGap(37, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {customerPartNumberText, customerText});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(auditTypeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel6)
                    .addComponent(customerText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(lotSizeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(qtyOrderedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7)
                    .addComponent(customerPartNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(sampleSizeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(partNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel8)
                    .addComponent(customerRevText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(inspectionLevelText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dieNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(operationSeqText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(shiftNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(revText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(orderlineText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {customerPartNumberText, customerRevText, customerText, jLabel10, jLabel6, jLabel7, jLabel8, jLabel9, operationSeqText, orderlineText});

        descText1.setEditable(true);

        jLabel19.setText("Description");

        descText2.setEditable(true);

        descText3.setEditable(true);

        descText4.setEditable(true);

        descText5.setEditable(true);

        descText6.setEditable(true);

        descText7.setEditable(true);

        descText8.setEditable(true);

        descText9.setEditable(true);

        descText10.setEditable(true);

        jLabel20.setText("Check Method");

        checkMethodText1.setEditable(true);

        checkMethodText2.setEditable(true);

        checkMethodText3.setEditable(true);

        checkMethodText4.setEditable(true);

        checkMethodText5.setEditable(true);

        checkMethodText6.setEditable(true);

        checkMethodText7.setEditable(true);

        checkMethodText8.setEditable(true);

        checkMethodText9.setEditable(true);

        checkMethodText10.setEditable(true);

        jLabel21.setText("Qty Sampled");

        jLabel18.setText("Qty Discrepant");

        defectDescText1.setEditable(true);

        defectDescText2.setEditable(true);

        defectDescText3.setEditable(true);

        defectDescText4.setEditable(true);

        defectDescText5.setEditable(true);

        defectDescText6.setEditable(true);

        defectDescText7.setEditable(true);

        defectDescText8.setEditable(true);

        defectDescText9.setEditable(true);

        defectDescText10.setEditable(true);

        jLabel15.setText("Defect Description");

        jLabel16.setText("Auditor Comments / Defect Summary");

        defectSummaryText.setColumns(20);
        defectSummaryText.setRows(5);
        jScrollPane2.setViewportView(defectSummaryText);

        dispositionText.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Accept", "Reject" }));

        jLabel17.setText("Disposition");

        jLabel22.setText("Auditor Name");

        jLabel23.setText("Closed Date");

        jLabel24.setText("Inspector No.");

        jLabel25.setText("DMR#");

        jButton1.setText("Submit Audit");
        jButton1.setName("submitAuditButton"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(dispositionText, 0, 100, Short.MAX_VALUE)
                                .addComponent(auditorNameText))
                            .addComponent(jLabel17)
                            .addComponent(jLabel22))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(closedDateText)
                                        .addComponent(inspNumberText, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                                    .addComponent(jLabel23))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel25)
                                    .addComponent(dmrNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel23)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dispositionText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(closedDateText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dmrNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(jLabel24))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(auditorNameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(inspNumberText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(descText9, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText8, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText7, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText6, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText5, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText4, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText3, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText1, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(descText10, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(checkMethodText9, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText8, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText7, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText6, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText5, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText4, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText3, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText1, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(checkMethodText10, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(qtySampledText2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(qtyDiscText2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(defectDescText2, 0, 1, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(qtySampledText3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(qtyDiscText3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(defectDescText3, 0, 1, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(qtySampledText4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(qtyDiscText4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(defectDescText4, 0, 1, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(qtySampledText5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(qtyDiscText5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(defectDescText5, 0, 1, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(qtySampledText6, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(qtyDiscText6, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(defectDescText6, 0, 1, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(qtySampledText7, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(qtyDiscText7, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(defectDescText7, 0, 1, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(qtySampledText8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(qtyDiscText8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(defectDescText8, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(qtySampledText9, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(qtyDiscText9, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(defectDescText9, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(qtySampledText10, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(qtyDiscText10, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(defectDescText10, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(qtySampledText1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel21))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(qtyDiscText1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel18))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(jLabel15)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(defectDescText1, 0, 1, Short.MAX_VALUE))))
                                .addGap(336, 336, 336))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21)
                    .addComponent(jLabel18)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(qtySampledText9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(descText9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(checkMethodText9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(qtyDiscText9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(defectDescText9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descText10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkMethodText10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtySampledText10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qtyDiscText10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(defectDescText10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {descText1, descText10, descText2, descText3, descText4, descText5, descText6, descText7, descText8, descText9});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {qtySampledText1, qtySampledText9});

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 647, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            
            String auditType = this.auditTypeText.getSelectedItem().toString();
            String qtyOrdered = this.qtyOrderedText.getText();
            String partNumber = this.partNumberText.getText();
            String dieNumber = this.dieNumberText.getText();
            String rev = this.revText.getText();
            String customer = this.customerText.getText();
            String customerPartNumber = this.customerPartNumberText.getText();
            String customerRev = this.customerRevText.getText();
            String orderline = this.orderlineText.getText();
            String operationSeq = this.operationSeqText.getText();
            String lotSize = this.lotSizeText.getText();
            String sampleSize = this.sampleSizeText.getSelectedItem().toString();
            String inspectionLevel = this.inspectionLevelText.getSelectedItem().toString();
            String shiftNumber = this.shiftNumberText.getText();
            
            String desc1 = this.descText1.getSelectedItem().toString();
            String desc2 = this.descText2.getSelectedItem().toString();
            String desc3 = this.descText3.getSelectedItem().toString();
            String desc4 = this.descText4.getSelectedItem().toString();
            String desc5 = this.descText5.getSelectedItem().toString();
            String desc6 = this.descText6.getSelectedItem().toString();
            String desc7 = this.descText7.getSelectedItem().toString();
            String desc8 = this.descText8.getSelectedItem().toString();
            String desc9 = this.descText9.getSelectedItem().toString();
            String desc10 = this.descText10.getSelectedItem().toString();
            
            String checkMethod1 = this.checkMethodText1.getSelectedItem().toString();
            String checkMethod2 = this.checkMethodText2.getSelectedItem().toString();
            String checkMethod3 = this.checkMethodText3.getSelectedItem().toString();
            String checkMethod4 = this.checkMethodText4.getSelectedItem().toString();
            String checkMethod5 = this.checkMethodText5.getSelectedItem().toString();
            String checkMethod6 = this.checkMethodText6.getSelectedItem().toString();
            String checkMethod7 = this.checkMethodText7.getSelectedItem().toString();
            String checkMethod8 = this.checkMethodText8.getSelectedItem().toString();
            String checkMethod9 = this.checkMethodText9.getSelectedItem().toString();
            String checkMethod10 = this.checkMethodText10.getSelectedItem().toString();
            
            String qtySampled1 = this.qtySampledText1.getText();
            String qtySampled2 = this.qtySampledText2.getText();
            String qtySampled3 = this.qtySampledText3.getText();
            String qtySampled4 = this.qtySampledText4.getText();
            String qtySampled5 = this.qtySampledText5.getText();
            String qtySampled6 = this.qtySampledText6.getText();
            String qtySampled7 = this.qtySampledText7.getText();
            String qtySampled8 = this.qtySampledText8.getText();
            String qtySampled9 = this.qtySampledText9.getText();
            String qtySampled10 = this.qtySampledText10.getText();
            
            String qtyDiscrepant1 = this.qtyDiscText1.getText();
            String qtyDiscrepant2 = this.qtyDiscText2.getText();
            String qtyDiscrepant3 = this.qtyDiscText3.getText();
            String qtyDiscrepant4 = this.qtyDiscText4.getText();
            String qtyDiscrepant5 = this.qtyDiscText5.getText();
            String qtyDiscrepant6 = this.qtyDiscText6.getText();
            String qtyDiscrepant7 = this.qtyDiscText7.getText();
            String qtyDiscrepant8 = this.qtyDiscText8.getText();
            String qtyDiscrepant9 = this.qtyDiscText9.getText();
            String qtyDiscrepant10 = this.qtyDiscText10.getText();
            
            String defectDescription1 = this.defectDescText1.getSelectedItem().toString();
            String defectDescription2 = this.defectDescText2.getSelectedItem().toString();
            String defectDescription3 = this.defectDescText3.getSelectedItem().toString();
            String defectDescription4 = this.defectDescText4.getSelectedItem().toString();
            String defectDescription5 = this.defectDescText5.getSelectedItem().toString();
            String defectDescription6 = this.defectDescText6.getSelectedItem().toString();
            String defectDescription7 = this.defectDescText7.getSelectedItem().toString();
            String defectDescription8 = this.defectDescText8.getSelectedItem().toString();
            String defectDescription9 = this.defectDescText9.getSelectedItem().toString();
            String defectDescription10 = this.defectDescText10.getSelectedItem().toString();
            
            String defectSummary = this.defectSummaryText.getText();
            String disposition = this.dispositionText.getSelectedItem().toString();
            String auditorName = this.auditorNameText.getText();
            String closedDate = this.closedDateText.getText();
            String inspNumber = this.inspNumberText.getText();
            String dmrNumber = this.dmrNumberText.getText();
            
            String now = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
                       
            SQLConnect conn = new SQLConnect();            
            con = conn.SQLConnect();
            stmt = con.createStatement();
                        
            String query = "Insert into tblQualityAudits (AuditType, Date, PartNumber, "  +
                    "Rev, InspectionLevel, Customer, ShopOrder, SampleSize, CustomerPartNumber, " +
                    "CustomerRev, OperationSeq, DieNumber, Lot_Size, ShiftNum, Description, Description2, " +
                    "Description3, Description4, Description5, Description6, Description7, Description8, " +
                    "Description9, Description10, CheckMethod1, CheckMethod2, CheckMethod3, CheckMethod4, " +
                    "CheckMethod5, CheckMethod6, CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10, " +
                    "DefectDescription1, DefectDescription2, DefectDescription3, DefectDescription4, DefectDescription5, " +
                    "DefectDescription6, DefectDescription7, DefectDescription8, DefectDescription9, DefectDescription10, " +
                    "AuditorDefectComments, Disposition, ClosedDate, DMRNum, OperNo, AuditorName, QtySampled, QtyDisc, " +
                    "QtySampled2, QtyDisc2, QtySampled3, QtyDisc3, QtySampled4, QtyDisc4, QtySampled5, QtyDisc5, QtySampled6, QtyDisc6, " +
                    "QtySampled7, QtyDisc7, QtySampled8, QtyDisc8, QtySampled9, QtyDisc9, QtySampled10, QtyDisc10) Select '" + auditType + "' as AuditType_Ins, '" 
                    + now + "' as Date_Ins, '" + partNumber + "' as PartNumber_Ins, '" + rev + "' as Rev_Ins, '" + inspectionLevel + "' as InspectionLevel_Ins, '" + customer + "' as Customer_Ins, '" + qtyOrdered + "' as ShopOrder_Ins, '" 
                    + sampleSize + "' as SampleSize_Ins, '" + customerPartNumber + "' as CustomerPartNumber_Ins, '" + customerRev + "' as CustomerRev_Ins, '" + operationSeq + "' as OperationSeq_Ins, '" 
                    + dieNumber + "' as DieNumber_Ins, '" + lotSize + "' as Lot_Size_Ins, '" + shiftNumber + "' as ShiftNum_Ins, '" + desc1 + "' as Description_Ins, '" 
                    + desc2 + "' as Description2_Ins, '" + desc3 + "' as Description3_Ins, '" + desc4 + "' as Descritpion4_Ins, '" + desc5 + "' as Description5_Ins, '" + desc6 + "' as Description6_Ins, '" 
                    + desc7 + "' as Description7_Ins, '" + desc8 + "' as Description8_Ins, '" + desc9 + "' as Description9_Ins, '" + desc10 + "' as Description10_Ins, '" 
                    + checkMethod1 + "' as CheckMethod1_Ins, '" + checkMethod2 + "' as CheckMethod2_Ins, '" + checkMethod3 + "' as CheckMethod3_Ins, '" + checkMethod4 + "' as CheckMethod4_Ins, '" + checkMethod5 + "' as CheckMethod5_Ins, '" 
                    + checkMethod6 + "' as CheckMethod6_Ins, '" + checkMethod7 + "' as CheckMethod7_Ins, '" + checkMethod8 + "' as CheckMethod8_Ins, '" + checkMethod9 + "' as CheckMethod9_Ins, '" + checkMethod10 + "' as CheckMethod10_Ins, '" 
                    + defectDescription1 + "' as DefectDescription1_Ins, '" + defectDescription2 + "' as DefectDescription2_Ins, '" + defectDescription3 + "' as DefectDescription3_Ins, '" + defectDescription4 + "' as DefectDescription4_Ins, '" 
                    + defectDescription5 + "' as DefectDescription5_Ins, '" + defectDescription6 + "' as DefectDescription6_Ins, '" + defectDescription7 + "' as DefectDescription7_Ins, '" + defectDescription8 + "' as DefectDescription8_Ins, '" 
                    + defectDescription9 + "' as DefectDescription9_Ins, '" + defectDescription10 + "' as DefectDescription10_Ins, '" + defectSummary + "' as AuditorDefectComments_Ins, '" 
                    + disposition + "' as Disposition_Ins, '" + closedDate + "' as ClosedDate_Ins, '" + dmrNumber + "' as DMRNum_Ins, '" + operationSeq + "' as OperNo_Ins, '" 
                    + auditorName + "' as AuditorName_Ins, '" + qtySampled1 + "' as QtySampled_Ins, '" + qtyDiscrepant1 + "' as QtyDisc_Ins, '" + qtySampled2 + "' as QtySampled2_Ins, '" 
                    + qtyDiscrepant2 + "' as QtyDisc2_Ins, '" + qtySampled3 + "' as QtySampled3_Ins, '" + qtyDiscrepant3 + "' as QtyDisc3_Ins, '" + qtySampled4 + "' as QtySampled4_Ins, '" 
                    + qtyDiscrepant4 + "' as QtyDisc4_Ins, '" + qtySampled5 + "' as QtySampled5_Ins, '" + qtyDiscrepant5 + "' as QtyDisc5_Ins, '" + qtySampled6 + "' as QtySampled6_Ins, '" 
                    + qtyDiscrepant6 + "' as QtyDisc6_Ins, '" + qtySampled7 + "' as QtySampled7_Ins, '" + qtyDiscrepant7 + "' as QtyDisc7_Ins, '" + qtySampled8 + "' as QtySampled8_Ins, '" 
                    + qtyDiscrepant8 + "' as QtyDisc8_Ins, '" + qtySampled9 + "' as QtySampled9_Ins, '" + qtyDiscrepant9 + "' as QtyDisc9_Ins, '" + qtySampled10 + "' as QtySampled10_Ins, '" 
                    + qtyDiscrepant10 + "' as QtyDisc10_Ins";          
            
            
            stmt.execute(query);
            stmt.close();
            con.close();
                                
        }
        catch (Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void searchRecipeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchRecipeButtonActionPerformed
        // TODO add your handling code here:
        try 
        {
            String woNum = workOrderNumberText.getText();
            String woLine = lineNumberText.getText();
            String woOper = operationNumberText.getText();
            
            
            ERPConnect oConn = new ERPConnect();
            con = oConn.ERPConnect();
            Statement stmnt = con.createStatement();
            String searchQuery = "Select "
                    + "WO.CCN As CCN, "
                    + "WO.MAS_LOC As ML, "
                    + "LTrim(WO.WO_NUM) As NumTrim, "
                    + "WO.WO_LINE As WOL, "
                    + "LTrim(WO_RTG.OPERATION) As OperTrim, "
                    + "WO.ORD_QTY As ORDQTY, "
                    + "WO.ITEM, "
                    + "WO.REVISION, "
                    + "BOM_HDR.BOM_DESCRIPTION, "
                    + "ITEM_CUS.CUSTOMER, "
                    + "ITEM_CUS.CUS_ITEM, "
                    + "ITEM_CUS.CUS_REV, "
                    + "BOM_HDR.BCR_TYPE, "
                    + "ITEM_CCN.USER1, "
                    + "ITEM_CCN.USER2"
                      + " From "
                    + "WO Inner Join "
                     + "WO_RTG On WO_RTG.CCN = WO.CCN "
                    + "And WO_RTG.MAS_LOC = WO.MAS_LOC "
                    + "And WO_RTG.WO_NUM = WO.WO_NUM "
                    + "And WO_RTG.WO_LINE = WO.WO_LINE Inner Join "
                    + "ITEM_CCN On ITEM_CCN.ITEM = WO.ITEM "
                    + "And ITEM_CCN.REVISION = WO.REVISION Inner Join "
                    + "BOM_HDR On BOM_HDR.ITEM = ITEM_CCN.ITEM "
                    + "And BOM_HDR.REVISION = ITEM_CCN.REVISION Left Join "
                    + "ITEM_CUS On ITEM_CUS.ITEM = BOM_HDR.ITEM "
                    + "And ITEM_CUS.REVISION = BOM_HDR.REVISION"
                      + " Where "
                    + "LTrim(WO.WO_NUM) = '" + woNum + "' And "
                    + "WO.WO_LINE = '" + woLine + "' And "
                    + "LTrim(WO_RTG.OPERATION) = '" + woOper + "' And "
                    + "BOM_HDR.BCR_TYPE = 'CUR' "
                    + "Group By "
                    + "WO.CCN, "
                    + "WO.MAS_LOC, "
                    + "LTrim(WO.WO_NUM), "
                    + "WO.WO_LINE, "
                    + "LTrim(WO_RTG.OPERATION), "
                    + "WO.ORD_QTY, "
                    + "WO.ITEM, "
                    + "WO.REVISION, "
                    + "BOM_HDR.BOM_DESCRIPTION, "
                    + "ITEM_CUS.CUSTOMER, "
                    + "ITEM_CUS.CUS_ITEM, "
                    + "ITEM_CUS.CUS_REV, "
                    + "BOM_HDR.BCR_TYPE, "
                    + "ITEM_CCN.USER1, "
                    + "ITEM_CCN.USER2";
            
            ResultSet rsSearch = stmnt.executeQuery(searchQuery);
            
            while (rsSearch.next()){ //fill header 
                
                qtyOrderedText.setText(rsSearch.getString("ORDQTY")); //qtyOrdered
                partNumberText.setText(rsSearch.getString("ITEM"));
                dieNumberText.setText(rsSearch.getString("USER2"));
                revText.setText(rsSearch.getString("REVISION"));
                customerText.setText(rsSearch.getString("CUSTOMER"));
                customerPartNumberText.setText(rsSearch.getString("CUS_ITEM"));
                customerRevText.setText(rsSearch.getString("CUS_REV"));
                operationNumberText.setText(rsSearch.getString("OperTrim"));
                orderlineText.setText(rsSearch.getString("WOL"));
                
                 geiItem = rsSearch.getString("ITEM");
                 geiRev = rsSearch.getString("REVISION");
                
            }
        
            con.close();
            stmnt.close();
            rsSearch.close();
            
        }
        catch (Exception ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex);
        }
        
        //Recipe Query
        try{
            SQLConnect connection = new SQLConnect();
            con = connection.SQLConnect();
            Statement statement = con.createStatement();
            String recipeQuery = "Select PartNumber, Rev, Description, Description2, Description3, Description4, Description5, Description6, "
                    + "Description7, Description8, Description9, Description10, CheckMethod, CheckMethod2, CheckMethod3, "
                    + "CheckMethod4, CheckMethod5, CheckMethod6, CheckMethod7, CheckMethod8, CheckMethod9, CheckMethod10 FROM tblAuditRecipe WHERE PartNumber = '"
                    + geiItem + "' and Rev = '" + geiRev + "'";
            
            ResultSet rsRecipe = statement.executeQuery(recipeQuery);
            
            while (rsRecipe.next()){ //fill form with queried recipe
                
                //Descriptions 1-10
                descText1.setSelectedItem(rsRecipe.getString("Description"));
                descText2.setSelectedItem(rsRecipe.getString("Description2"));
                descText3.setSelectedItem(rsRecipe.getString("Description3"));
                descText4.setSelectedItem(rsRecipe.getString("Description4"));
                descText5.setSelectedItem(rsRecipe.getString("Description5"));
                descText6.setSelectedItem(rsRecipe.getString("Description6"));
                descText7.setSelectedItem(rsRecipe.getString("Description7"));
                descText8.setSelectedItem(rsRecipe.getString("Description8"));
                descText9.setSelectedItem(rsRecipe.getString("Description9"));
                descText10.setSelectedItem(rsRecipe.getString("Description10"));
                        
                //CheckMethods 1-10
                checkMethodText1.setSelectedItem(rsRecipe.getString("CheckMethod")); 
                checkMethodText2.setSelectedItem(rsRecipe.getString("CheckMethod2"));
                checkMethodText3.setSelectedItem(rsRecipe.getString("CheckMethod3"));
                checkMethodText4.setSelectedItem(rsRecipe.getString("CheckMethod4"));
                checkMethodText5.setSelectedItem(rsRecipe.getString("CheckMethod5"));
                checkMethodText6.setSelectedItem(rsRecipe.getString("CheckMethod6"));
                checkMethodText7.setSelectedItem(rsRecipe.getString("CheckMethod7"));
                checkMethodText8.setSelectedItem(rsRecipe.getString("CheckMethod8"));
                checkMethodText9.setSelectedItem(rsRecipe.getString("CheckMethod9"));
                checkMethodText10.setSelectedItem(rsRecipe.getString("CheckMethod10"));
                                                                  
            }
            
            con.close();
            statement.close();
            rsRecipe.close();
            
        }
        catch (Exception x){
            x.printStackTrace();
            JOptionPane.showMessageDialog(null, x);
        }
        
    }//GEN-LAST:event_searchRecipeButtonActionPerformed
    
    private void reportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportButtonActionPerformed
        // TODO add your handling code here:
       
        SQLConnect conn = new SQLConnect();
        String imgPath = "C:\\Program Files\\ProductAudit\\lib\\geiBrandBoot.png";
        String geiDrawingPath = null;        
        String report = "auditLabels.jasper";   
        InputStream reportFile = null;
        
        String dieNumber = dieNumberText.getText();
        String geiPartNumber = partNumberText.getText();
        String odlPartNumberParam = null;
        String imgQuery = "SELECT ImageAttachment, CertifyingText from tblAuditRecipe WHERE PartNumber = '" + geiPartNumber +"'";
        Map hm = new HashMap();        
        
        try{ 
                    
            con = conn.SQLConnect();
            Statement stmt = con.createStatement();
            ResultSet rsImage = stmt.executeQuery(imgQuery);
            
                
                while (rsImage.next()){                
                    InputStream inputStream = rsImage.getBinaryStream(1);
                    
                    
                    if (inputStream != null){                                          
                        BufferedImage Image = ImageIO.read(inputStream);   
                        hm.put("geiDrawing", Image);
                        
                    }
                    else {
                        
                    }
                    
                    odlPartNumberParam = rsImage.getString("CertifyingText");
                    
                }
            

            String partNumberParam = partNumberText.getText();
            String woNumberParam = workOrderNumberText.getText();
            String lineNumberParam = lineNumberText.getText();
            String auditorNameParam = System.getenv("USERNAME");
            
            hm.put("partNumber", partNumberParam); //Hash map to pass param values      
            hm.put("woNum", woNumberParam);
            hm.put("lineNum", lineNumberParam);
            hm.put("auditorName", auditorNameParam);           
            hm.put("odlPartNumber", odlPartNumberParam);
            hm.put("imgPath", imgPath);
            
            
            reportFile = getClass().getResourceAsStream(report);
           // JasperReport jr = JasperCompileManager.compileReport(reportPath);
            JasperPrint jp = JasperFillManager.fillReport(reportFile, hm, new JREmptyDataSource());
            JasperViewer.viewReport(jp, false); // 2nd argument is CLOSEPARENTONEXIT
            con.close();
            stmt.close();
            rsImage.close();
        }
        catch (Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e);
        }
        
        
    }//GEN-LAST:event_reportButtonActionPerformed

    private void newRecipeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newRecipeButtonActionPerformed
        // TODO add your handling code here:
        //New Recipe
        this.dispose();
        new NewRecipe().setVisible(true);        
    }//GEN-LAST:event_newRecipeButtonActionPerformed

    private void auditViewerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_auditViewerButtonActionPerformed
        // TODO add your handling code here:
        //Audit Viewer
        new AuditViewer().setVisible(true);
    }//GEN-LAST:event_auditViewerButtonActionPerformed

    private void printAuditButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printAuditButtonActionPerformed
        // TODO add your handling code here:
        //Print audit
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setJobName(" Print Component ");

        pj.setPrintable (new Printable() {    
          public int print(Graphics pg, PageFormat pf, int pageNum){
            if (pageNum > 0){
            return Printable.NO_SUCH_PAGE;
            }

            Graphics2D g2 = (Graphics2D) pg;
            g2.translate(pf.getImageableX(), pf.getImageableY());
            jPanel6.paint(g2);
            return Printable.PAGE_EXISTS;
          }
        });
        if (pj.printDialog() == false)
        return;

        try {
              pj.print();
        } catch (PrinterException ex) {
              // handle exception
              ex.printStackTrace();
              JOptionPane.showMessageDialog(null, ex);
        }
        
    }//GEN-LAST:event_printAuditButtonActionPerformed

    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AuditForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AuditForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AuditForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AuditForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AuditForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> auditTypeText;
    private javax.swing.JButton auditViewerButton;
    private javax.swing.JTextField auditorNameText;
    private javax.swing.JComboBox<String> checkMethodText1;
    private javax.swing.JComboBox<String> checkMethodText10;
    private javax.swing.JComboBox<String> checkMethodText2;
    private javax.swing.JComboBox<String> checkMethodText3;
    private javax.swing.JComboBox<String> checkMethodText4;
    private javax.swing.JComboBox<String> checkMethodText5;
    private javax.swing.JComboBox<String> checkMethodText6;
    private javax.swing.JComboBox<String> checkMethodText7;
    private javax.swing.JComboBox<String> checkMethodText8;
    private javax.swing.JComboBox<String> checkMethodText9;
    private javax.swing.JTextField closedDateText;
    private javax.swing.JTextField customerPartNumberText;
    private javax.swing.JTextField customerRevText;
    private javax.swing.JTextField customerText;
    private javax.swing.JComboBox<String> defectDescText1;
    private javax.swing.JComboBox<String> defectDescText10;
    private javax.swing.JComboBox<String> defectDescText2;
    private javax.swing.JComboBox<String> defectDescText3;
    private javax.swing.JComboBox<String> defectDescText4;
    private javax.swing.JComboBox<String> defectDescText5;
    private javax.swing.JComboBox<String> defectDescText6;
    private javax.swing.JComboBox<String> defectDescText7;
    private javax.swing.JComboBox<String> defectDescText8;
    private javax.swing.JComboBox<String> defectDescText9;
    private javax.swing.JTextArea defectSummaryText;
    private javax.swing.JComboBox<String> descText1;
    private javax.swing.JComboBox<String> descText10;
    private javax.swing.JComboBox<String> descText2;
    private javax.swing.JComboBox<String> descText3;
    private javax.swing.JComboBox<String> descText4;
    private javax.swing.JComboBox<String> descText5;
    private javax.swing.JComboBox<String> descText6;
    private javax.swing.JComboBox<String> descText7;
    private javax.swing.JComboBox<String> descText8;
    private javax.swing.JComboBox<String> descText9;
    private javax.swing.JTextField dieNumberText;
    private javax.swing.JComboBox<String> dispositionText;
    private javax.swing.JTextField dmrNumberText;
    private javax.swing.JTextField inspNumberText;
    private javax.swing.JComboBox<String> inspectionLevelText;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField lineNumberText;
    private javax.swing.JTextField lotSizeText;
    private javax.swing.JButton newRecipeButton;
    private javax.swing.JTextField operationNumberText;
    private javax.swing.JTextField operationSeqText;
    private javax.swing.JTextField orderlineText;
    private javax.swing.JTextField partNumberText;
    private javax.swing.JButton printAuditButton;
    private javax.swing.JTextField qtyDiscText1;
    private javax.swing.JTextField qtyDiscText10;
    private javax.swing.JTextField qtyDiscText2;
    private javax.swing.JTextField qtyDiscText3;
    private javax.swing.JTextField qtyDiscText4;
    private javax.swing.JTextField qtyDiscText5;
    private javax.swing.JTextField qtyDiscText6;
    private javax.swing.JTextField qtyDiscText7;
    private javax.swing.JTextField qtyDiscText8;
    private javax.swing.JTextField qtyDiscText9;
    private javax.swing.JTextField qtyOrderedText;
    private javax.swing.JTextField qtySampledText1;
    private javax.swing.JTextField qtySampledText10;
    private javax.swing.JTextField qtySampledText2;
    private javax.swing.JTextField qtySampledText3;
    private javax.swing.JTextField qtySampledText4;
    private javax.swing.JTextField qtySampledText5;
    private javax.swing.JTextField qtySampledText6;
    private javax.swing.JTextField qtySampledText7;
    private javax.swing.JTextField qtySampledText8;
    private javax.swing.JTextField qtySampledText9;
    private javax.swing.JButton reportButton;
    private javax.swing.JTextField revText;
    private javax.swing.JComboBox<String> sampleSizeText;
    private javax.swing.JButton searchRecipeButton;
    private javax.swing.JTextField shiftNumberText;
    private javax.swing.JTextField workOrderNumberText;
    // End of variables declaration//GEN-END:variables
}
