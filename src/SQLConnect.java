/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tbarrett
 */
import java.sql.Connection;
import java.sql.DriverManager;

public class SQLConnect {
    public Connection SQLConnect(){
        Connection conn = null; 
        try {
            
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); 
            //jdbc:sqlserver://dbHost\sqlexpress;user=sa;password=secret
            String connectionUrl = "jdbc:sqlserver://GEI-SQL1:1433;databaseName=GEI_PRODUCT_AUDITS;integratedSecurity=true";
            conn = DriverManager.getConnection(connectionUrl);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
    
}
