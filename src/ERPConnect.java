/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tbarrett
 */

import java.sql.Connection;
import java.sql.DriverManager;

public class ERPConnect {
    public Connection ERPConnect() {
        final String DEFAULT_URL = "jdbc:oracle:thin:@10.218.63.20:1521/G2PROD.gei.local";
        final String DEFAULT_USERNAME = "glovia_prod";
        final String DEFAULT_PASSWORD = "manager";

        Connection conn = null;

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(DEFAULT_URL, DEFAULT_USERNAME, DEFAULT_PASSWORD);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
}